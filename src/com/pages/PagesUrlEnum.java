package com.pages;

public enum PagesUrlEnum {
	LOGIN("https://salonboard.com/login/","SALON BOARD : ログイン"),// controller
	CUSTOMER_SEARCH("https://salonboard.com/CLP/bt/customer/customerSearch/search","SALON BOARD : お客様情報一覧"),// controller
	CUSTOMER_DETAIL("https://salonboard.com/CLP/bt/customer/customerDetail/?customerId=","SALON BOARD : お客様情報詳細"),// crawler
	SALES_DETAIL_SEARCH("https://salonboard.com/CLP/bt/sales/salesDetailSearch/","SALON BOARD : 売上明細"),// controller

	;

	private String pageUrl;
	private String pageTitle;

	private PagesUrlEnum(String pageUrl, String pageTitle){
		this.pageUrl = pageUrl;
		this.pageTitle = pageTitle;
	}

	public String getPageUrl() {
		return this.pageUrl;
	}

	public String getPageTitle() {
		return this.pageTitle;
	}
}

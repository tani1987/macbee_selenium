package com.pages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.pages.model.IPageModel;
import com.pages.model.impl.SalesDetailPageModel;
import com.util.DbManager;
import com.util.UserParams;


public class ComingHistoryDao{

	private static final String INSERT_SQL = "INSERT INTO histories " +
			" (user_id, customer_id, coming_date, return_period)" +
			" VALUES (?, ?, ?, ?)";

	public void insertSalesInfo(String customerId, String comingDate, String returnPeriod) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(INSERT_SQL);

			ps.setInt(1, UserParams.getUserId());
			ps.setString(2, customerId);
			ps.setString(3, comingDate);
			ps.setString(4, returnPeriod);

			ps.execute();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
	}
}

package com.pages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.pages.model.IPageModel;
import com.pages.model.impl.CustomerDetailModel;
import com.util.DbManager;
import com.util.UserParams;

public class CustomerSearchPageCheckDao {

	private static final String SELECT_SQL = "SELECT customer_id FROM customers;";
	public Set<String> getCutomerIdSet() throws SQLException {
		Set<String> customerIdSet = Sets.newHashSet();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			rs = ps.executeQuery();
			while(rs.next()){
				customerIdSet.add(rs.getString(1));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
		return customerIdSet;
	}
}

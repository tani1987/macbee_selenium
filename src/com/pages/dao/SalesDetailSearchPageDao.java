package com.pages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.pages.model.IPageModel;
import com.pages.model.impl.SalesDetailPageModel;
import com.util.DbManager;
import com.util.UserParams;


public class SalesDetailSearchPageDao{

	private static final String INSERT_SQL = "INSERT INTO sales " +
			" (user_id, accounting_date, accounting_time, accounting_id, accounting_division, division, category" +
			", menu, unit_price, price_division, number, price" +
			", stylist, nomination, treatment_person, name_kanji, customer_number, name_kana" +
			", reservation_route, gender, new_or_again)" +
			" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public void insertSalesInfo(List<IPageModel> modelList) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(INSERT_SQL);
			ps.setFetchSize(10000);
			for (int i = 0; i < modelList.size(); i++) {
				SalesDetailPageModel model = (SalesDetailPageModel) modelList.get(i);

				ps.setInt(1, UserParams.getUserId());
				ps.setString(2, model.getAccountingDate());
				ps.setString(3, model.getAccountingTime());
				ps.setString(4, model.getAccountingId());
				ps.setString(5, model.getAccountingDivision());
				ps.setString(6, model.getDivision());
				ps.setString(7, model.getCategory());
				ps.setString(8, model.getMenu());
				ps.setString(9, model.getUnitPrice());
				ps.setString(10, model.getPriceDivision());
				ps.setString(11, model.getNumber());
				ps.setString(12, model.getPrice());
				ps.setString(13, model.getStylist());
				ps.setString(14, model.getNomination());
				ps.setString(15, model.getTreatmentPerson());
				ps.setString(16, model.getNameKanji());
				ps.setString(17, model.getCustomerNumber());
				ps.setString(18, model.getNameKana());
				ps.setString(19, model.getReservationRoute());
				ps.setString(20, model.getGender());
				ps.setString(21, model.getNewOrAgain());

				ps.addBatch();
			}
			ps.executeBatch();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
	}
}

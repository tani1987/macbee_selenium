package com.pages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.util.DbManager;

public class UserParamsDao {

	private static final String SELECT_SQL = "SELECT id FROM users WHERE user_name = ? and password = ?;";
	public int getUserName(String userName, String userPwd) throws SQLException {
		int userId = 0;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(SELECT_SQL);
			ps.setString(1, userName);
			ps.setString(2, userPwd);
			rs = ps.executeQuery();
			while(rs.next()){
				userId = rs.getInt("id");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
		return userId;
	}
}

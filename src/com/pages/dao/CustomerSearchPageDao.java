package com.pages.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.pages.model.IPageModel;
import com.pages.model.impl.CustomerDetailModel;
import com.util.DbManager;
import com.util.UserParams;

public class CustomerSearchPageDao {
	private static final String INSERT_BASE_INFO_SQL = "INSERT INTO customers " +
			" (customer_id, name_kana, name_kanji, telephone_1, telephone_2, email_pc, email_mobile, customer_number, address, available_letter" +
			", birthday, blood_type, work, gender, caution, customer_memo, other_1, other_2, other_3, first_coming_store_date" +
			", coming_store_reason, coming_store_times, message_destination, message_destination_email_pc, message_destination_email_mobile,user_id)" +
			" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
			", ?, ?, ?, ?, ?, ?)";

	private static final String INSERT_RESERVATION_INFO_SQL = "INSERT INTO histories " +
			" (user_id, customer_id, coming_date, interval, next_date)" +
			" VALUES (?, ?, ?, ?, ?)";

	public void insertCustomerInfo(List<IPageModel> modelList) throws SQLException {
		insertBaseInfo(modelList);
		insertCustomerReservationInfo(modelList);
	}

	private void insertBaseInfo(List<IPageModel> modelList) throws SQLException{

		Connection conn = null;
		PreparedStatement ps = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(INSERT_BASE_INFO_SQL);
			ps.setFetchSize(20000);
			for (int i = 0; i < modelList.size(); i++) {
				CustomerDetailModel model = (CustomerDetailModel) modelList.get(i);

				ps.setString(1, model.getCustomerId());
				ps.setString(2, model.getNameKanji());
				ps.setString(3, model.getNameKana());
				ps.setString(4, model.getTelephoneNumber1());
				ps.setString(5, model.getTelephoneNumber2());
				ps.setString(6, model.getEmailPc());
				ps.setString(7, model.getEmailMobile());
				ps.setString(8, model.getCustomerNumber());
				ps.setString(9, model.getAddress());
				ps.setString(10, model.getAvailableLetter());
				ps.setString(11, model.getBirthday());
				ps.setString(12, model.getBloodType());
				ps.setString(13, model.getWork());
				ps.setString(14, model.getGender());
				ps.setString(15, model.getCaution());
				ps.setString(16, model.getCustomerMemo());
				ps.setString(17, model.getOther1());
				ps.setString(18, model.getOther2());
				ps.setString(19, model.getOther3());
				ps.setString(20, model.getFirstComingStoreDate());
				ps.setString(21, model.getComingStoreReason());
				ps.setString(22, model.getComingStoreTimes());
				ps.setString(23, model.getMessageDestination());
				ps.setString(24, model.getMessageDestinationEmailPc());
				ps.setString(25, model.getMessageDestinationEmailMobile());
				ps.setInt(26, UserParams.getUserId());

				ps.addBatch();
			}
			ps.executeBatch();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
	}

	private void insertCustomerReservationInfo(List<IPageModel> modelList) throws SQLException{
		Connection conn = null;
		PreparedStatement ps = null;

		try{
			conn = DbManager.getConnection();
			ps = conn.prepareStatement(INSERT_RESERVATION_INFO_SQL);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			for (int i = 0; i < modelList.size(); i++) {
				CustomerDetailModel model = (CustomerDetailModel) modelList.get(i);
				ps.setInt(1, UserParams.getUserId());
				ps.setString(2, model.getCustomerId());

				String comingDataCommaText = model.getComingDate();
				ps.setString(3, comingDataCommaText);


				long differenceDays = 0;
				int interval = 45;// just default value
				Calendar previousDate = Calendar.getInstance();;
				Calendar latestDate = Calendar.getInstance();;

				if(comingDataCommaText != null && !comingDataCommaText.isEmpty()){
					String[] commingDates = comingDataCommaText.split(",");
					for (int j = 0; j < commingDates.length; j++) {
						String[] dateParams = commingDates[j].split("/");
						Calendar nextDate =  Calendar.getInstance();
						nextDate.set(Integer.parseInt(dateParams[0]), Integer.parseInt(dateParams[1]), Integer.parseInt(dateParams[2]));
						if(previousDate == null){
							previousDate = nextDate;
							latestDate = nextDate;
						}else{
							differenceDays = differenceDays + ( previousDate.getTimeInMillis() - nextDate.getTimeInMillis() ) / (24 * 60 * 60 * 1000 );
						}
					}
					if(differenceDays != 0){
						interval = (int) (differenceDays / commingDates.length);
					}
				}
				ps.setInt(4, interval);
				latestDate.add(Calendar.DATE, interval);
				ps.setString(5, sdf.format(latestDate.getTime()));
				ps.addBatch();
			}
			ps.executeBatch();

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DbManager.closeResources(conn, ps);
		}
	}

	private int getInterval(Date previousDate,Date nextDate){
	    long previousDateTime = previousDate.getTime();
	    long nextDateTime = nextDate.getTime();
	    long one_date_time = 1000 * 60 * 60 * 24;
	    long diffDays = (previousDateTime - nextDateTime) / one_date_time;
	    return (int)diffDays;

	}
}
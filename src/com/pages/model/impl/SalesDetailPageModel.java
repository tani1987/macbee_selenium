package com.pages.model.impl;

import com.orangesignal.csv.annotation.CsvColumn;
import com.orangesignal.csv.annotation.CsvEntity;
import com.pages.model.IPageModel;

@CsvEntity
public class SalesDetailPageModel implements IPageModel {

	@CsvColumn(position = 0, name = "会計日")
    private String accountingDate;
    @CsvColumn(position = 1, name = "会計時間")
    private String accountingTime;
    @CsvColumn(position = 2, name = "会計ID")
    private String accountingId;
    @CsvColumn(position = 3, name = "会計区分")
    private String accountingDivision;
    @CsvColumn(position = 4, name = "区分")
    private String division;
    @CsvColumn(position = 5, name = "カテゴリ")
    private String category;
    @CsvColumn(position = 6, name = "メニュー・店販・ 割引・サービス・オプション")
    private String menu;
    @CsvColumn(position = 7, name = "単価")
    private String unitPrice;
    @CsvColumn(position = 8, name = "単価区分")
    private String priceDivision;
    @CsvColumn(position = 9, name = "個数")
    private String number;
    @CsvColumn(position = 10, name = "金額")
    private String price;
    @CsvColumn(position = 11, name = "担当スタイリスト")
    private String stylist;
    @CsvColumn(position = 12, name = "指名")
    private String nomination;
    @CsvColumn(position = 13, name = "施術担当者")
    private String treatmentPerson;
    @CsvColumn(position = 14, name = "お客様名")
    private String nameKanji;// CustomerDetailModel
    @CsvColumn(position = 15, name = "お客様番号")
    private String customerNumber;// CustomerDetailModel
    @CsvColumn(position = 16, name = "お客様名（フリガナ）")
    private String nameKana;// CustomerDetailModel
    @CsvColumn(position = 17, name = "予約経路")
    private String reservationRoute;
    @CsvColumn(position = 18, name = "性別")
    private String gender;// CustomerDetailModel
    @CsvColumn(position = 19, name = "新規再来")
    private String newOrAgain;



	public String getAccountingDate() {
		return accountingDate;
	}
	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}
	public String getAccountingTime() {
		return accountingTime;
	}
	public void setAccountingTime(String accountingTime) {
		this.accountingTime = accountingTime;
	}
	public String getAccountingId() {
		return accountingId;
	}
	public void setAccountingId(String accountingId) {
		this.accountingId = accountingId;
	}
	public String getAccountingDivision() {
		return accountingDivision;
	}
	public void setAccountingDivision(String accountingDivision) {
		this.accountingDivision = accountingDivision;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getPriceDivision() {
		return priceDivision;
	}
	public void setPriceDivision(String priceDivision) {
		this.priceDivision = priceDivision;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getStylist() {
		return stylist;
	}
	public void setStylist(String stylist) {
		this.stylist = stylist;
	}
	public String getNomination() {
		return nomination;
	}
	public void setNomination(String nomination) {
		this.nomination = nomination;
	}
	public String getTreatmentPerson() {
		return treatmentPerson;
	}
	public void setTreatmentPerson(String treatmentPerson) {
		this.treatmentPerson = treatmentPerson;
	}
	public String getNameKanji() {
		return nameKanji;
	}
	public void setNameKanji(String nameKanji) {
		this.nameKanji = nameKanji;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getNameKana() {
		return nameKana;
	}
	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}
	public String getReservationRoute() {
		return reservationRoute;
	}
	public void setReservationRoute(String reservationRoute) {
		this.reservationRoute = reservationRoute;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNewOrAgain() {
		return newOrAgain;
	}
	public void setNewOrAgain(String newOrAgain) {
		this.newOrAgain = newOrAgain;
	}

}

package com.pages.model.impl;

import com.pages.model.IPageModel;

public class CustomerDetailModel implements IPageModel {
	private String customerId;
	private String nameKanji;
	private String nameKana;
	private String telephoneNumber1;
	private String telephoneNumber2;
	private String emailPc;
	private String emailMobile;
	private String customerNumber;
	private String address;
	private String availableLetter;
	private String birthday;
	private String bloodType;
	private String work;
	private String gender;
	private String caution;
	private String customerMemo;
	private String other1;
	private String other2;
	private String other3;
	private String firstComingStoreDate;
	private String comingStoreReason;
	private String comingStoreTimes;
	private String messageDestination;
	private String messageDestinationEmailPc;
	private String messageDestinationEmailMobile;
	private String messageDestinationHPBMyPage;
	private String messageDestinationHPBMailAddress;

	public String getComingDate() {
		return comingDate;
	}

	public void setComingDate(String comingDate) {// need creating comma text
		if(this.comingDate == null){
			this.comingDate = comingDate;
		}else{
			this.comingDate = new String(new StringBuilder().append(this.comingDate).append(",").append(comingDate));
		}
	}


	private String comingDate;

	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getNameKanji() {
		return nameKanji;
	}
	public void setNameKanji(String nameKanji) {
		this.nameKanji = nameKanji;
	}
	public String getNameKana() {
		return nameKana;
	}
	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}
	public String getTelephoneNumber1() {
		return telephoneNumber1;
	}
	public void setTelephoneNumber1(String telephoneNumber1) {
		this.telephoneNumber1 = telephoneNumber1;
	}
	public String getTelephoneNumber2() {
		return telephoneNumber2;
	}
	public void setTelephoneNumber2(String telephoneNumber2) {
		this.telephoneNumber2 = telephoneNumber2;
	}
	public String getEmailPc() {
		return emailPc;
	}
	public void setEmailPc(String emailPc) {
		this.emailPc = emailPc;
	}
	public String getEmailMobile() {
		return emailMobile;
	}
	public void setEmailMobile(String emailMobile) {
		this.emailMobile = emailMobile;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAvailableLetter() {
		return availableLetter;
	}
	public void setAvailableLetter(String availableLetter) {
		this.availableLetter = availableLetter;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getBloodType() {
		return bloodType;
	}
	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCaution() {
		return caution;
	}
	public void setCaution(String caution) {
		this.caution = caution;
	}
	public String getCustomerMemo() {
		return customerMemo;
	}
	public void setCustomerMemo(String customerMemo) {
		this.customerMemo = customerMemo;
	}
	public String getOther1() {
		return other1;
	}
	public void setOther1(String other1) {
		this.other1 = other1;
	}
	public String getOther2() {
		return other2;
	}
	public void setOther2(String other2) {
		this.other2 = other2;
	}
	public String getOther3() {
		return other3;
	}
	public void setOther3(String other3) {
		this.other3 = other3;
	}
	public String getFirstComingStoreDate() {
		return firstComingStoreDate;
	}
	public void setFirstComingStoreDate(String firstComingStoreDate) {
		this.firstComingStoreDate = firstComingStoreDate;
	}

	public String getComingStoreReason() {
		return comingStoreReason;
	}
	public void setComingStoreReason(String comingStoreReason) {
		this.comingStoreReason = comingStoreReason;
	}
	public String getComingStoreTimes() {
		return comingStoreTimes;
	}
	public void setComingStoreTimes(String comingStoreTimes) {
		this.comingStoreTimes = comingStoreTimes;
	}
	public String getMessageDestination() {
		return messageDestination;
	}
	public void setMessageDestination(String messageDestination) {
		this.messageDestination = messageDestination;
	}
	public String getMessageDestinationEmailPc() {
		return messageDestinationEmailPc;
	}
	public void setMessageDestinationEmailPc(String messageDestinationEmailPc) {
		this.messageDestinationEmailPc = messageDestinationEmailPc;
	}
	public String getMessageDestinationEmailMobile() {
		return messageDestinationEmailMobile;
	}
	public void setMessageDestinationEmailMobile(
			String messageDestinationEmailMobile) {
		this.messageDestinationEmailMobile = messageDestinationEmailMobile;
	}
	public String getMessageDestinationHPBMyPage() {
		return messageDestinationHPBMyPage;
	}
	public void setMessageDestinationHPBMyPage(String messageDestinationHPBMyPage) {
		this.messageDestinationHPBMyPage = messageDestinationHPBMyPage;
	}
	public String getMessageDestinationHPBMailAddress() {
		return messageDestinationHPBMailAddress;
	}
	public void setMessageDestinationHPBMailAddress(
			String messageDestinationHPBMailAddress) {
		this.messageDestinationHPBMailAddress = messageDestinationHPBMailAddress;
	}
}

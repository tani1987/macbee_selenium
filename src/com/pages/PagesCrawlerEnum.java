package com.pages;

import org.openqa.selenium.WebDriver;
import com.pages.crawler.AbsPageCrawler;
import com.pages.crawler.impl.CustomerDetailCrawler;
import com.pages.model.impl.CustomerDetailModel;

public enum PagesCrawlerEnum {
	CUSTOMER_DETAIL(PagesUrlEnum.CUSTOMER_DETAIL) {
		@Override
		public AbsPageCrawler getCrawler(WebDriver driver, String pageUrl) {
			return new CustomerDetailCrawler(driver ,pageUrl, this.pagesUrlEnum.getPageTitle(), new CustomerDetailModel());
		}
	}
	;

	protected PagesUrlEnum pagesUrlEnum;

	private PagesCrawlerEnum(PagesUrlEnum pagesUrlEnum){
		this.pagesUrlEnum = pagesUrlEnum;
	}

	public abstract AbsPageCrawler getCrawler(WebDriver driver, String pageUrl);

	public String getPageUrl() {
		return this.pagesUrlEnum.getPageUrl();
	}

	public String getPageTitle() {
		return this.pagesUrlEnum.getPageTitle();
	}
}

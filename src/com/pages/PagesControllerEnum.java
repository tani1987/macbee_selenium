package com.pages;

import org.openqa.selenium.WebDriver;

import com.pages.controller.AbsPageController;
import com.pages.controller.impl.CustomerSearchPageController;
import com.pages.controller.impl.LoginPageController;
import com.pages.controller.impl.SalesDetailSearchPageController;

// This enum includes all page cotroller
public enum PagesControllerEnum {
	LOGIN(PagesUrlEnum.LOGIN) {
		@Override
		public AbsPageController getController(WebDriver driver) {
			return new LoginPageController(driver, this.pagesUrlEnum.getPageUrl(), this.pagesUrlEnum.getPageTitle());
		}
	},CUSTOMER_SEARCH(PagesUrlEnum.CUSTOMER_SEARCH) {
		@Override
		public AbsPageController getController(WebDriver driver) {
			return new CustomerSearchPageController(driver ,this.pagesUrlEnum.getPageUrl(), this.pagesUrlEnum.getPageTitle());
		}
	},SALES_DETAIL_SEARCH(PagesUrlEnum.SALES_DETAIL_SEARCH) {
		@Override
		public AbsPageController getController(WebDriver driver) {
			return new SalesDetailSearchPageController(driver ,this.pagesUrlEnum.getPageUrl(), this.pagesUrlEnum.getPageTitle());
		}
	}
	;

	protected PagesUrlEnum pagesUrlEnum;

	private PagesControllerEnum(PagesUrlEnum pagesUrlEnum){
		this.pagesUrlEnum = pagesUrlEnum;
	}

	public abstract AbsPageController getController(WebDriver driver);

	public String getPageUrl() {
		return this.pagesUrlEnum.getPageUrl();
	}

	public String getPageTitle() {
		return this.pagesUrlEnum.getPageTitle();
	}
}

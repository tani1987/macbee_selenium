package com.pages.controller;

import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.Lists;
import com.pages.model.IPageModel;

public abstract class AbsPageController implements IPageController {

	protected final WebDriver driver;
	private WebDriverWait wait;

	protected AbsPageController(WebDriver driver, String pageUrl, String pageTitle){
		driver.get(pageUrl);
		wait = new WebDriverWait(driver, 10); // 秒数は適当。
		wait.until(ExpectedConditions.titleIs(pageTitle));
		this.driver = driver;
	}

	public void init() throws Exception{
		List <IPageModel> modelList = Lists.newArrayList();
		initAction(modelList);
		insertData(modelList);
	}

	public void update() throws Exception{
		List <IPageModel> modelList = Lists.newArrayList();
		updateAction(modelList);
		updateData(modelList);
	}

	@Override
	public abstract void initAction(List <IPageModel> modelList) throws Exception;
	@Override
	public abstract void updateAction(List <IPageModel> modelList) throws Exception;
	@Override
	public abstract void insertData(List <IPageModel> modelList) throws SQLException;
	@Override
	public abstract void updateData(List <IPageModel> modelList) throws SQLException;

}

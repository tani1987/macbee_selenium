package com.pages.controller;


import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.pages.PagesControllerEnum;
import com.pages.PagesCrawlerEnum;
import com.pages.crawler.AbsPageCrawler;

public class PageMaster {

	private static WebDriver driver;
	private static String baseUrl;
	// private static String DRIVER_TYPE = "webdriver.chrome.driver";
	//TODO: later this DRIVER_PATH should be loaded by conf file b/c it is different according to OS of user server.
	// private static String DRIVER_PATH = "./driver/chromedriver";

	static {
//		System.setProperty(DRIVER_TYPE, DRIVER_PATH);
        // プロファイルの作成
		FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 0); // ダウンロードするファイルの保存先フォルダを指定　0:デスクトップ 1：ダウンロードフォルダ 2:ダウンロードに指定された最後のフォルダ
        profile.setPreference("browser.download.useDownloadDir", true); // ダウンロードするファイルの保存先フォルダが指定してあればそれを使う
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/text;"); // 指定したmimeタイプは有無を言わさずダウンロードする
        driver = new FirefoxDriver(profile); // 作成したプロファイルでFirefox(のドライバー)を起動する


//		WebDriver webDriver = new ChromeDriver();
//		 driver = webDriver;
		baseUrl = "https://salonboard.com";
	}

	public static final AbsPageController getPageController(PagesControllerEnum pageController){
		return pageController.getController(driver);
	}

	public static final AbsPageCrawler getPageCrawler(PagesCrawlerEnum pagesCrawler, String pageUrl){
		return pagesCrawler.getCrawler(driver, pageUrl);
	}


	public static final void init(){
		try {
			PageMaster.getPageController(PagesControllerEnum.LOGIN).init();
			PageMaster.getPageController(PagesControllerEnum.CUSTOMER_SEARCH).init();
			PageMaster.getPageController(PagesControllerEnum.SALES_DETAIL_SEARCH).init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final String getBaseUrl(){
		return baseUrl;
	}

	public static final void quit(){
		driver.quit();
	}
}

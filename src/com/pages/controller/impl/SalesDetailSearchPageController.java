package com.pages.controller.impl;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipFile;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.collect.Lists;
import com.orangesignal.csv.Csv;
import com.orangesignal.csv.CsvConfig;
import com.orangesignal.csv.handlers.BeanListHandler;
import com.orangesignal.csv.handlers.ColumnNameMappingBeanListHandler;
import com.orangesignal.csv.handlers.CsvEntityListHandler;
import com.orangesignal.csv.handlers.StringArrayListHandler;
import com.orangesignal.csv.manager.CsvEntityManager;
import com.orangesignal.csv.manager.CsvManager;
import com.orangesignal.csv.manager.CsvManagerFactory;
import com.pages.controller.AbsPageController;
import com.pages.dao.CustomerSearchPageDao;
import com.pages.dao.SalesDetailSearchPageDao;
import com.pages.model.IPageModel;
import com.pages.model.impl.SalesDetailPageModel;
import com.util.CalendarUtil;
import com.util.UserParams;

public class SalesDetailSearchPageController extends AbsPageController {

	public SalesDetailSearchPageController(WebDriver driver, String pageUrl, String pageTitle) {
		super(driver, pageUrl, pageTitle);
	}

	@Override
	public void initAction(List <IPageModel> modelList) throws Exception{
		// init時にはあらかじめexportされたcsvファイルが指定の場所にあり、それを読み込んでimportする形になる

		//TODO:明細zipファイルが配置されている絶対パスをinit時に取得
        File files[] = new File(UserParams.getCsvDir()).listFiles();

        for (File file : files) {
        	Charset encord = Charset.forName("Shift_JIS");
    		CsvConfig cfg = new CsvConfig(',', '"', '"');
    		cfg.setIgnoreEmptyLines(true);

            List<SalesDetailPageModel> csvModelList = Lists.newArrayListWithExpectedSize(15000);
        	if(file.getName().endsWith(".zip")){
        		ZipFile zipFile = new ZipFile(file, encord);
        		csvModelList = new CsvEntityManager(cfg).load(SalesDetailPageModel.class).from(zipFile, encord.toString());
       		}else{
        		csvModelList = new CsvEntityManager(cfg).load(SalesDetailPageModel.class).from(file, encord.toString());
       		}
    		modelList.addAll(csvModelList);
       	}

        updateAction(modelList);
	}

	@Override
	public void updateAction(List<IPageModel> modelList) {

		displayCurrentMonth();
		crawlAction(modelList);
	}

	private void crawlAction(List <IPageModel> modelList){

		String nextPageUrl = null;
		try{
			WebElement nextButton = driver.findElement(By.cssSelector(".next"));
			nextPageUrl = nextButton.findElement(By.tagName("a")).getAttribute("href");
		}catch(Exception e){// NoSuchElementException
			// crush exception b/c this should be ignored
		}

		List<WebElement> trElements = driver.findElements(By.cssSelector("table.dmtabletype01 tbody tr"));

		for (WebElement trElement : trElements) {
			SalesDetailPageModel model = new SalesDetailPageModel();
			List<WebElement> tdElements = trElement.findElements(By.tagName("td"));
			for (int i = 0; i < tdElements.size(); i++) {
				switch(i){
				case 0:
					String nameAndAccountingDate = tdElements.get(i).getText();
					String[] params = nameAndAccountingDate.split("\n");
					model.setNameKanji(params[0]);
					model.setAccountingDate(params[1]);
					break;
				case 1:
					model.setDivision(tdElements.get(i).getText());
					break;
				case 2:
					model.setCategory(tdElements.get(i).getText());
					break;
				case 3:
					model.setMenu(tdElements.get(i).getText());
					break;
				case 4:
					model.setStylist(tdElements.get(i).getText());
					break;
				case 5:
					model.setTreatmentPerson(tdElements.get(i).getText());
					break;
				case 6:
					model.setUnitPrice(tdElements.get(i).getText());
					break;
				case 7:
					model.setNumber(tdElements.get(i).getText());
					break;
				case 8:
					model.setPrice(tdElements.get(i).getText());
					break;
				default:
					break;
				}
			}
			modelList.add(model);
		}

		if(nextPageUrl != null){
			this.driver.get(nextPageUrl);
			crawlAction(modelList);
		}
	}

	private void displayCurrentMonth(){
		Calendar currentCalendar = Calendar.getInstance();
		Calendar startMaonthCalendar = Calendar.getInstance();
		Calendar endMonthCalendar = Calendar.getInstance();

		startMaonthCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), 1, 0, 0, 0);

		endMonthCalendar.set(currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), 1, 0, 0, 0);
		endMonthCalendar.add(Calendar.MONTH, 1);
		endMonthCalendar.set(Calendar.MILLISECOND, 0);
		endMonthCalendar.add(Calendar.MILLISECOND, -1);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String startDate = sdf.format(startMaonthCalendar.getTime());
        String endDate = sdf.format(endMonthCalendar.getTime());

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		// make hidden input boxes visibly
		jse.executeScript("document.getElementsByName('searchDateFrom')[0].setAttribute('type', 'text');");
		jse.executeScript("document.getElementsByName('searchDateTo')[0].setAttribute('type', 'text');");

		WebElement searchDateFromElement = driver.findElement(By.name("searchDateFrom"));
		WebElement searchDateToformElement = driver.findElement(By.name("searchDateTo"));

		searchDateFromElement.clear();
		searchDateToformElement.clear();
		searchDateFromElement.sendKeys(startDate);
		searchDateToformElement.sendKeys(endDate);

		driver.findElement(By.linkText("検索する")).click();
	}



	@Override
	public void insertData(List <IPageModel> modelList) throws SQLException {
		new SalesDetailSearchPageDao().insertSalesInfo(modelList);
	}

	@Override
	public void updateData(List<IPageModel> modelList) throws SQLException {
		new SalesDetailSearchPageDao().insertSalesInfo(modelList);
	}
}

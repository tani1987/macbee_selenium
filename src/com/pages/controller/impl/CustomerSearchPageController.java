package com.pages.controller.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.collect.Lists;
import com.pages.PagesCrawlerEnum;
import com.pages.controller.AbsPageController;
import com.pages.crawler.IPageCrawler;
import com.pages.crawler.impl.CustomerDetailCrawler;
import com.pages.dao.CustomerSearchPageCheckDao;
import com.pages.dao.CustomerSearchPageDao;
import com.pages.dao.UserParamsDao;
import com.pages.model.IPageModel;

public class CustomerSearchPageController extends AbsPageController {

	public CustomerSearchPageController(WebDriver driver, String pageUrl, String pageTitle) {
		super(driver, pageUrl, pageTitle);
	}

	@Override
	public void initAction(List <IPageModel> modelList){
		crawlAction(modelList);
	}

	private Set<String> customerSet;

	@Override
	public void updateAction(List<IPageModel> modelList) throws SQLException {
		this.customerSet = new CustomerSearchPageCheckDao().getCutomerIdSet();

		List <WebElement> customerDetailLinkList = driver.findElements(By.cssSelector("table.customerInfoTbl td a"));
		List<String> customerDetailUrlList = Lists.newArrayList();

		for (WebElement customerDetailLink : customerDetailLinkList) {
			String linkText = customerDetailLink.getAttribute("href");
			if(!this.customerSet.contains(linkText.substring(linkText.length()-12))){// pick up only new targets for registeration.
				customerDetailUrlList.add(linkText);
			}
		}

		for (String customerDetailUrl : customerDetailUrlList) {
			CustomerDetailCrawler pageCrawler = (CustomerDetailCrawler)(PagesCrawlerEnum.CUSTOMER_DETAIL.getCrawler(driver, customerDetailUrl));
			pageCrawler.crawl(modelList);
		}

		WebElement nextButton = driver.findElement(By.cssSelector(".next"));
		String nextPageUrl = nextButton.findElement(By.tagName("a")).getAttribute("href");

		if(nextPageUrl != null){
			this.driver.get(nextPageUrl);
			updateAction(modelList);
		}
	}

	private void crawlAction(List <IPageModel> modelList){
		List <WebElement> customerDetailLinkList = driver.findElements(By.cssSelector("table.customerInfoTbl td a"));
		// Need to create deep copied objects before moving next pages.
		WebElement nextButton = driver.findElement(By.cssSelector(".next"));
		String nextPageUrl = nextButton.findElement(By.tagName("a")).getAttribute("href");

		List<String> customerDetailUrlList = Lists.newArrayList();
		for (WebElement customerDetailLink : customerDetailLinkList) {
			customerDetailUrlList.add(customerDetailLink.getAttribute("href"));
		}

		for (String customerDetailUrl : customerDetailUrlList) {
			CustomerDetailCrawler pageCrawler = (CustomerDetailCrawler)(PagesCrawlerEnum.CUSTOMER_DETAIL.getCrawler(driver, customerDetailUrl));
//			modelList.add(pageCrawler.crawl());
			pageCrawler.crawl(modelList);

			//			if(modelList.size() > 5){//TODO: Remove later b/c this if branch is just for testing!
//				return;
//			}
		}


//		if(nextPageUrl != null){
//			this.driver.get(nextPageUrl);
//			initAction(modelList);
		//}
	}

	@Override
	public void insertData(List <IPageModel> modelList) throws SQLException {
		registerCustomer(modelList);
	}

	@Override
	public void updateData(List<IPageModel> modelList) throws SQLException {
		registerCustomer(modelList);
	}

	private void registerCustomer(List<IPageModel> modelList) throws SQLException{
		new CustomerSearchPageDao().insertCustomerInfo(modelList);
	}
}

package com.pages.controller.impl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pages.controller.AbsPageController;
import com.pages.model.IPageModel;
import com.util.UserParams;

public class LoginPageController extends AbsPageController {

	public LoginPageController(WebDriver driver, String pageUrl, String pageTitle) {
		super(driver, pageUrl,pageTitle);
	}


	@Override
	public void initAction(List<IPageModel> modelList) {
		login();
	}

	@Override
	public void updateAction(List<IPageModel> modelList) {
		login();
	}

	private void login(){
		WebElement idInput = driver.findElement(By.name("userId"));
		idInput.sendKeys(UserParams.getHotPepperUserId());
		WebElement pwdInput = driver.findElement(By.name("password"));
		pwdInput.sendKeys(UserParams.getHotPepperMainPwd());
		WebElement loginButton = driver.findElement(By.linkText("ログイン"));
		loginButton.click();
		//TODO: ログイン失敗時にpwd2を用いて再度ログインを試みるロジック
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	@Override
	public void insertData(List<IPageModel> modelList) {
		// nothing to do
	}

	@Override
	public void updateData(List<IPageModel> modelList) {
		// nothing to do
	}
}

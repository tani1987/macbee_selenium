package com.pages.controller;

import java.sql.SQLException;
import java.util.List;

import com.pages.model.IPageModel;

public interface IPageController {
	public abstract void initAction(List <IPageModel> modelList) throws Exception;
	public abstract void updateAction(List <IPageModel> modelList) throws Exception;
	public abstract void insertData(List <IPageModel> modelList) throws SQLException;
	public abstract void updateData(List <IPageModel> modeloList) throws SQLException;
}

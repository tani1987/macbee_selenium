package com.pages.crawler;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pages.model.IPageModel;

public abstract class AbsPageCrawler implements IPageCrawler {

	protected final WebDriver driver;
	private WebDriverWait wait;
	private IPageModel model;

	protected AbsPageCrawler(WebDriver driver, String pageUrl, String pageTitle, IPageModel model){
		driver.get(pageUrl);
		wait = new WebDriverWait(driver, 10); // 秒数は適当。
		wait.until(ExpectedConditions.titleIs(pageTitle));
		this.driver = driver;

		this.model = model;
	}

	/**
	 * Use this method with CAST
	 */
	public IPageModel getModel(){
		return this.model;
	}

	@Override
	public abstract IPageModel crawl();

	protected abstract void setValue(String colName, String colValue);
}

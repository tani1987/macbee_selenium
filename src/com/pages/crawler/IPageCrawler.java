package com.pages.crawler;

import com.pages.model.IPageModel;

public interface IPageCrawler {
	public IPageModel crawl();
	public IPageModel getModel();
}

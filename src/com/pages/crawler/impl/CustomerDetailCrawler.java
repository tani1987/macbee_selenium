package com.pages.crawler.impl;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.pages.crawler.AbsPageCrawler;
import com.pages.model.IPageModel;
import com.pages.model.impl.CustomerDetailModel;

public class CustomerDetailCrawler extends AbsPageCrawler{

	public CustomerDetailCrawler(WebDriver driver, String pageUrl,
			String pageTitle, IPageModel model) {
		super(driver, pageUrl, pageTitle, model);
	}

	private static final String CUSTOMER_ID_DESCRIPTION = "顧客ID:";
	private CustomerDetailModel model;

	@Override
	public IPageModel crawl() {
		this.model = (CustomerDetailModel) getModel();

		String customerIdText = driver.findElement(By.cssSelector("#customerDetail > .mod_align_right")).getText();
		this.model.setCustomerId(customerIdText.replaceFirst(CUSTOMER_ID_DESCRIPTION, ""));

		WebElement formElement = driver.findElement(By.cssSelector("#customerDetail"));
		// make them multi thread
		List<WebElement> tableElemnts = formElement.findElements(By.tagName("table"));
		for (WebElement tableElemnt : tableElemnts) {
			List<WebElement> trElemnts = tableElemnt.findElements(By.cssSelector("tr"));
			for (WebElement trElemnt : trElemnts) {
				String keyText = trElemnt.findElement(By.cssSelector("th")).getText();
				if("来店日".equals(keyText) // Ignore "予約ページ"　in this page
						|| "配信日".equals(keyText)){// Ignore "メッセージ配信履歴"　in this page
					break;
				}
				setValue(keyText,trElemnt.findElement(By.cssSelector("td")).getText());
			}
		}
		return model;
	}

	private List<Map> trList;

	public void crawl (final List <IPageModel> modelList){
		this.model = (CustomerDetailModel) getModel();
		modelList.add(this.model);
		String customerIdText = driver.findElement(By.cssSelector("#customerDetail > .mod_align_right")).getText();
		this.model.setCustomerId(customerIdText.replaceFirst(CUSTOMER_ID_DESCRIPTION, ""));

		WebElement formElement = driver.findElement(By.cssSelector("#customerDetail"));
		List<WebElement> tableElemnts = formElement.findElements(By.tagName("table"));

		List<WebElement> trElements = Lists.newArrayList();
		for (WebElement tableElement : tableElemnts) {
			trElements.addAll(tableElement.findElements(By.cssSelector("tr")));
		}
		trList = Lists.newArrayList();

		for (WebElement trElement : trElements) {
			Map<String, String> trMap = Maps.newHashMap();
			try{
				String keyText = trElement.findElement(By.cssSelector("th")).getText();
				String valueText = trElement.findElement(By.cssSelector("td")).getText();
				trMap.put("key", keyText);
				trMap.put("value", valueText);
				trList.add(trMap);
			}catch(NoSuchElementException e){
				// ignore some items like "予約履歴" or something
			}
		}

		// getting reservation histories
			List<WebElement> reservationHistoriesTrList = driver.findElements(By.cssSelector(".treatmentTbl tbody tr"));
			for (WebElement reservationHistoriesTrElement : reservationHistoriesTrList) {
				this.model.setComingDate(reservationHistoriesTrElement.findElement(By.tagName("td")).getText().substring(0,10));
			}

		Runnable runnable = new Runnable() {
			public void run() {
				for (Map<String, String> trMap : trList) {
					setValue(trMap.get("key"), trMap.get("value"));
				}
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}

	@Override
	public void setValue(String colName, String colValue){
		switch(colName){
		case "氏名 (漢字)":
			this.model.setNameKanji(colValue);
			break;
		case "氏名 (カナ)":
			this.model.setNameKana(colValue);
			break;
		case "電話番号1":
			this.model.setTelephoneNumber1(colValue);
			break;
		case "電話番号2":
			this.model.setTelephoneNumber2(colValue);
			break;
		case "E-MAIL (PC)":
			if(this.model.getEmailPc() == null){
				this.model.setEmailPc(colValue);
			}else{
				this.model.setMessageDestinationEmailPc(colValue);
			}
			break;
		case "E-MAIL (携帯)":
			this.model.setEmailMobile(colValue);
			break;
		case "お客様番号":
			this.model.setCustomerNumber(colValue);
			break;
		case "住所":
			this.model.setAddress(colValue);
			break;
		case "はがき送付許諾":
			this.model.setAvailableLetter(colValue);
			break;
		case "誕生日":
			this.model.setBirthday(colValue);
			break;
		case "性別":
			this.model.setGender(colValue);
			break;
		case "血液型":
			this.model.setBloodType(colValue);
			break;
		case "職業":
			this.model.setWork(colValue);
			break;
		case "要注意チェック":
			this.model.setCaution(colValue);
			break;
		case "お客様メモ":
			this.model.setCustomerMemo(colValue);
			break;
		case "その他1":
			this.model.setOther1(colValue);
			break;
		case "その他2":
			this.model.setOther2(colValue);
			break;
		case "その他3":
			this.model.setOther3(colValue);
			break;
		case "初回来店日":
			this.model.setFirstComingStoreDate(colValue);
			break;
		case "来店きっかけ":
			this.model.setComingStoreReason(colValue);
			break;
		case "来店回数":
			this.model.setComingStoreTimes(colValue);
			break;
		case "メッセージ配信先":
			this.model.setMessageDestination(colValue);
			break;
		case " ":// because of fucking design!!
			this.model.setMessageDestinationEmailPc(colValue);
			break;
		case "E-MAIL(携帯)":
			this.model.setMessageDestinationEmailMobile(colValue);
			break;
		case "Hot Pepper Beautyマイページ":
			this.model.setMessageDestinationEmailMobile(colValue);
			break;
		case "Hot Pepper Beautyメールアドレス":
			this.model.setMessageDestinationEmailMobile(colValue);
			break;
		default:
			break;
		}
	}
}
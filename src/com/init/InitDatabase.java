package com.init;

import java.sql.SQLException;

import com.pages.controller.PageMaster;
import com.pages.dao.UserParamsDao;
import com.util.UserParams;

public class InitDatabase {

	public static void main(String[] args) {

		initInfo(args);
		
		if(0 != UserParams.getUserId()){
		PageMaster.init();
		PageMaster.quit();
		}else{
			System.out.println("ユーザー名 または パスワードが誤っています");
		}
	}

	private static void initInfo(String[] args){
		UserParams.setUserName(args[0]);
		UserParams.setUserPwd(args[1]);
		UserParams.setHotPepperUserId(args[2]);
		UserParams.setHotPepperMainPwd(args[3]);
		UserParams.setHotPepperSubPwd(args[4]);
		UserParams.setCsvDir(args[5]);

		try {
			UserParams.setUserId(new UserParamsDao().getUserName(UserParams.getUserName(), UserParams.getUserPwd()));
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}

package com.util;

public class UserParams {
	private static int userId;
	private static String userName;
	private static String userPwd;
	private static String hotPepperUserId;
	private static String hotPepperMainPwd;
	private static String hotPepperSubPwd;
	private static String csvDir;


	public static String getUserName() {
		return userName;
	}
	public static void setUserName(String userName) {
		UserParams.userName = userName;
	}

	public static int getUserId() {
		return userId;
	}
	public static void setUserId(int userId) {
		UserParams.userId = userId;
	}
	public static String getUserPwd() {
		return userPwd;
	}
	public static void setUserPwd(String userPwd) {
		UserParams.userPwd = userPwd;
	}
	public static String getHotPepperUserId() {
		return hotPepperUserId;
	}
	public static void setHotPepperUserId(String hotPepperUserId) {
		UserParams.hotPepperUserId = hotPepperUserId;
	}
	public static String getHotPepperMainPwd() {
		return hotPepperMainPwd;
	}
	public static void setHotPepperMainPwd(String hotPepperMainPwd) {
		UserParams.hotPepperMainPwd = hotPepperMainPwd;
	}
	public static String getHotPepperSubPwd() {
		return hotPepperSubPwd;
	}
	public static void setHotPepperSubPwd(String hotPepperSubPwd) {
		UserParams.hotPepperSubPwd = hotPepperSubPwd;
	}
	public static String getCsvDir() {
		return csvDir;
	}
	public static void setCsvDir(String csvDir) {
		UserParams.csvDir = csvDir;
	}

}

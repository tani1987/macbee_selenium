package com.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbManager {
    private static String driverName = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://54.149.202.82/macbee?useUnicode=true&characterEncoding=utf8";
    private static String user = "tani1987";
    private static String pass = "tani1987";

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url,user,pass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void closeResources(Connection conn) throws SQLException{
    	closeResources(conn, null, null);
    }

    public static void closeResources(Connection conn, PreparedStatement ps) throws SQLException{
    	closeResources(conn, ps, null);
    }

    public static void closeResources(Connection conn, PreparedStatement ps, ResultSet rs) throws SQLException{
    	if(rs != null){
        	rs.close();
    	}
    	if(ps != null){
        	ps.close();
    	}
    	if(conn != null){
        	conn.close();

    	}
    }

}

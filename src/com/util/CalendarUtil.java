package com.util;

import java.util.Calendar;

public class CalendarUtil {

	public static Calendar getFirstDayOfMonth(){
	    Calendar c=Calendar.getInstance();
//	    c.set(Calendar.YEAR,year);
//	    c.set(Calendar.MARCH,month);

	    int days=c.getActualMinimum(Calendar.DAY_OF_MONTH);
	    c.set(Calendar.DAY_OF_MONTH,days);
	    return c;
	}

	public static Calendar getLastDayOfMonth(){
	    Calendar c=Calendar.getInstance();
//	    c.set(Calendar.YEAR,year);
//	    c.set(Calendar.MARCH,month);

	    int days=c.getActualMaximum(Calendar.MONTH);
	    c.set(Calendar.DAY_OF_MONTH,days);
	    return c;
	}
}
